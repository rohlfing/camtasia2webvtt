#! /usr/bin/env python
# coding: utf8

"""A tool to grab chapter information from Camtasia confix XML file and convert it to WebVTT Chapter file"""

# Copyright (c) 2019 Christian Rohlfing
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import xml.etree.ElementTree as ET  # walk the xml tree
import os  # filename magic
import argparse  # parse input arguments
import sys

__version__ = '0.1'
__author__ = 'Christian Rohlfing'
__license__ = 'MIT'


# Namespaces as specified in RDF
RDF_NS = '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}'
XML_NS = '{http://www.w3.org/XML/1998/namespace}'
namespaces = {"x": "adobe:ns:meta/",
              "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
              "xmp": "http://ns.adobe.com/xap/1.0/",
              "xmpDM": "http://ns.adobe.com/xmp/1.0/DynamicMedia/",
              "xmpG": "http://ns.adobe.com/xap/1.0/g/",
              "tsc": "http://www.techsmith.com/xmp/tsc/",
              "xmpMM": "http://ns.adobe.com/xap/1.0/mm/",
              "tscDM": "http://www.techsmith.com/xmp/tscDM/",
              "tscIQ": "http://www.techsmith.com/xmp/tscIQ/",
              "tscHS": "http://www.techsmith.com/xmp/tscHS/",
              "stDim": "http://ns.adobe.com/xap/1.0/sType/Dimensions#",
              "stFnt": "http://ns.adobe.com/xap/1.0/sType/Font#",
              "exif": "http://ns.adobe.com/exif/1.0",
              "dc": "http://purl.org/dc/elements/1.1/"}


def s_to_timestamp(total_seconds):
    """
    Convert time in seconds to hh:mm:sss format (as used in WebVTT).
    Taken from https://github.com/glut23/webvtt-py
    """
    hours = int(total_seconds / 3600)
    minutes = int(total_seconds / 60 - hours * 60)
    seconds = total_seconds - hours * 3600 - minutes * 60
    return '{:02d}:{:02d}:{:06.3f}'.format(hours, minutes, seconds)


def main():
    """Main function."""
    # Set file-encoding to UTF8
    if sys.version_info[0] < 3:
        reload(sys)
        sys.setdefaultencoding('utf-8')

    # Parse input arguments
    parser = argparse.ArgumentParser(description="Grab chapter information from Camtasia config XML file and write it to WebVTT file")
    parser.add_argument("infile", type=str,
                        help="Camtasia config xml file (should end with '_config.xml')")
    parser.add_argument("-n", "--note", type=str,
                        default="RWTH Aachen University",
                        help="Additional note to be written to header of WEBVTT file. Default: 'RWTH Aachen University'.")
    parser.add_argument("-v", "--verbosity", action="count", default=0, help="increase output verbosity")
    args = parser.parse_args()

    xml_filename = args.infile
    note = args.note

    # Start XML parsing
    print("Converting " + xml_filename)
    webvtt_filename = os.path.splitext(xml_filename)[0] + '.vtt'
    tree = ET.parse(xml_filename)
    root = tree.getroot()

    # Grab table of contents (TOC) from xml file
    toc = root.findall('.//rdf:Description[@xmpDM:trackType="TableOfContents"]', namespaces)
    if len(toc) > 1:
        raise NameError("Mehr als ein Element 'TableOfContents'!")
    toc = toc[0]
    tocentries = toc.findall('.//rdf:Description', namespaces)

    # Grab total duration from xml file
    tmp = root.findall('.//xmpDM:duration', namespaces)[0]
    totalLength = tmp.attrib["{"+namespaces['xmpDM']+"}value"]

    # Write header to output string
    s = 'WEBVTT\n\n'

    # Write note if given
    if note:
        s += 'NOTE\n' + note + '\n\n'

    # Write TOC entries to output string
    for it, e in enumerate(tocentries):
        # Grab start time and chapter name
        startTime = int(e.attrib["{"+namespaces['xmpDM']+"}startTime"])
        chapName = e.attrib["{"+namespaces['xmpDM']+"}name"]

        # Grab stop time. Either start time of next entry
        # or total length if current TOC entry is last one
        if it < len(tocentries)-1:  # all but last entries
            # get start time of next entry which is stop time of current entry
            eNext = tocentries[it+1]
            stopTime = int(eNext.attrib["{"+namespaces['xmpDM']+"}startTime"])
        else:  # last entry: stop time is total track length
            stopTime = int(totalLength)

        # Convert times (in milliseconds) to seconds
        # and then to formatted VTT string
        startTimeFormatted = s_to_timestamp(startTime/1000)
        stopTimeFormatted = s_to_timestamp(stopTime/1000)

        # Add VTT entry to output string
        s += "{}\n{} --> {}\n{}\n\n".format(it+1,
                                                  startTimeFormatted,
                                                  stopTimeFormatted,
                                                  chapName)

    # Save output string to vtt file
    f = open(webvtt_filename, 'w')
    f.writelines(s)
    f.close()

    print("Done.")

if __name__ == "__main__":
    sys.exit(main())
