# Camtasia2WebVTT

A tool to grab chapter information from Camtasia confix XML file and convert it to WebVTT Chapter file.

## Usage

```bash
usage: camtasia2webvtt.py [-h] [-n NOTES] [-v] infile

Grab chapter information from Camtasia config XML file and write it to WebVTT file

positional arguments:
  infile                Camtasia config xml file (should end with '_config.xml')

optional arguments:
  -h, --help            show this help message and exit
  -n NOTE, --note NOTE
                        Additional note to be written to header of WEBVTT file. Default: 'RWTH Aachen University'.
```

Examples:

```bash
camtasia2webvtt.py test_config.xml
```

yields `test_config.vtt`, the converted WebVTT chapter file.

```bash
camtasia2webvtt.py test_config.xml -n "IENT RWTH Aachen University"
```

adds `IENT RWTH Aachen University` as `NOTE` to the header.

## WebVTT

Example taken out of the [W3C WebVTT Specification](https://www.w3.org/TR/webvtt1/#introduction-chapters)

```
WEBVTT

NOTE
This is from a talk Silvia gave about WebVTT.

Slide 1
00:00:00.000 --> 00:00:10.700
Title Slide

Slide 2
00:00:10.700 --> 00:00:47.600
Introduction by Naomi Black

Slide 3
00:00:47.600 --> 00:01:50.100
Impact of Captions on the Web

Slide 4
00:01:50.100 --> 00:03:33.000
Requirements of a Video text format
```


## Installation

Python installation is needed. Tested with Python 3.6.7.

## Contact

Please feel free to contact me, Christian Rohlfing, at <rohlfing@ient.rwth-aachen.de>.

## License

Licensed under the term of `MIT License`. See attached file `LICENSE`.
